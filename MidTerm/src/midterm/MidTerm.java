/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midterm;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author pn
 */
public class MidTerm {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Subject java2 = new Subject("Java 2");
        HashMap<Integer, Student> newSet = new HashMap<Integer, Student>();
        LinkedHashSet<Student> tempStudentSet = new LinkedHashSet<Student>();
        LinkedList<Student> tempStudentList = new LinkedList<Student>();

        Student s1 = new Student(121, "Jecson", "Caracut");
        Student s2 = new Student(126, "Wilmar", "Arguedo");
        Student s3 = new Student(122, "Khim", "Cole");
        Student s4 = new Student(123, "Kabadra", "Abra");
        
        java2.addStudent(s1);
        java2.addStudent(s2);
        java2.addStudent(s3);
        java2.addStudent(s3);
        java2.addStudent(s4);


        Iterator<Student> itr = java2.getStudents().iterator();

        while (itr.hasNext()) {

            tempStudentSet.add((Student) itr.next());
        }

        itr = tempStudentSet.iterator();

        int ctr = 0;
        while (itr.hasNext()) {
            Student temp = (Student) itr.next();

            if (tempStudentList.isEmpty()) {
                tempStudentList.add(temp);
            } else {
                ctr = 0;
                for (Student stud : tempStudentList) {
                    if (temp.getLastName().charAt(0) < stud.getLastName().charAt(0)) {
                        break;
                    } else if (temp.getLastName().charAt(1) < stud.getLastName().charAt(1)) {
                        break;
                    } else if (temp.getLastName().charAt(2) < stud.getLastName().charAt(2)) {
                        break;
                    } else if (temp.getLastName().charAt(3) < stud.getLastName().charAt(3)) {
                        break;
                    }
                    ctr++;
                }

                tempStudentList.add(ctr, temp);
            }

        }
        ctr = 0;
        for (Student stud: tempStudentList) {
            System.out.println("Student " + ++ctr);
            System.out.println("Name : " + stud.getCompleteName());
            System.out.println("ID Number : " + stud.getId() + "\n");
        }   
        for (Student student : tempStudentList) {
            newSet.put(student.getId(), student);
        }

        ctr = 0;
        
        System.out.println("HASHMAP\n");
        for (Map.Entry m : newSet.entrySet()) {
            System.out.println("Student " + ++ctr);
            Student temp = (Student) m.getValue();
            System.out.println("Name : " + temp.getCompleteName());
            System.out.println("ID Number : " + temp.getId() + "\n");
        }
    }
}
